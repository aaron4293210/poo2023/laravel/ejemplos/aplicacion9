<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class modalborrado extends Component {
    public $idModal;
    public $mensaje;
    public $ruta;

    public function __construct($idModal, $mensaje, $ruta) {
        $this->idModal = $idModal;
        $this->mensaje = $mensaje;
        $this->ruta = $ruta;
    }

    public function render(): View|Closure|string {
        return view('components.mios.modalborrado');
    }
}
