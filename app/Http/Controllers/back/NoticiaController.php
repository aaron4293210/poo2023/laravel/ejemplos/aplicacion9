<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoticiaStoreRequest;
use App\Http\Requests\NoticiaUpdateRequest;
use App\Models\Noticia;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NoticiaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        // Auth -> Facades de laravel
        // Auth::check(); // Devuelve true o false si el usuario esta autenticado o no.
        // Auth::guest(); // Devuelve true o false si el usuario no esta autenticado o no.
        // Auth::user(); // Devuelve el usuario actualmente autenticado.
        // Auth::user()->user_id; // Devuelve el usuario actualmente autenticado.
        // Auth::user()->name; // Devuelve el nombre usuario actualmente autenticado.

        // auth()->check(); // Devuelve true o false si el usuario esta autenticado o no.
        // auth()->user(); // Devuelve el usuario actualmente autenticado.
        // auth()->user()->user_id; // Devuelve el usuario actualmente autenticado.

        // Obtengo todas las noticias de los usuarios
        $noticias = Noticia::paginate(8);

        // Redireciona a la vista back.index con el usuario logueado
        return view('back.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
    */
    public function create() {
        return view('back.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NoticiaStoreRequest $request) {
        $fotoEnviada = $request->file('foto');

        if ($fotoEnviada) {
            $foto = $fotoEnviada->store('fotos', 'public');
        }
        else {
            $foto = null;
        }

        $noticia = new Noticia();
            $noticia->fill($request->all());
            $noticia->foto = $foto;
            $noticia->user_id = Auth::user()->id;
        $noticia->save();

        return redirect()
            ->route('back.noticias.show', $noticia)
            ->with('mensaje', 'Noticia se ha creado con éxito');
    }

    /**
     * Display the specified resource.
     */
    public function show(Noticia $noticia) {
        return view('back.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Noticia $noticia) {
        if ($noticia->user_id != Auth::user()->id) {
            abort(403, 'No tienes permisos para editar esta noticia');
        }

        return view('back.noticias.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NoticiaUpdateRequest $request, Noticia $noticia) {
        if ($request->hasFile('foto')) {
            $fotoEnviada = $request->file('foto');
            Storage::disk('public')->delete($noticia->foto);

            $nuevaFoto = $fotoEnviada->store('fotos', 'public');

            $noticia->fill($request->all());
            $noticia->foto = $nuevaFoto;
            $noticia->user_id = Auth::user()->id;
        }
        else {
            $noticia->update($request->all());
        }

        $noticia->save();

        return redirect()
            ->route('back.noticias.show', $noticia)
            ->with('mensaje', 'Noticia actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Noticia $noticia) {
        if ($noticia->user_id != Auth::user()->id) {
            abort(403, 'No tienes permisos para borrar esta noticia');
        }

        $fotoEnviada = $noticia->foto;
        Storage::disk('public')->delete($fotoEnviada);

        $noticia->delete();

        return redirect()
            ->route('back.noticias.listado')
            ->with('mensaje', 'Noticia borrada con éxito')
        ;
    }

    public function listado() {
        $noticias = Noticia::where('user_id', Auth::user()->id)->paginate(8);

        return view('back.noticias.listado', compact('noticias'));
    }

    public function searchNoticia(Request $request) {
        // Validar el parámetro de búsqueda
        $validated = $request->validate([
            'query' => 'required|string|max:255',
        ]);

        $query = $validated['query'];

        // Ejecutar la búsqueda de noticias con paginación
        $noticias = Noticia::where('titulo', 'like', '%' . $query . '%')->paginate(8);

        // Agregar el parámetro de consulta a la paginación
        $noticias->appends(['query' => $query]);

        // Retornar la vista con los resultados de la búsqueda
        return view('back.noticias.listado', compact('noticias'));
    }
}
