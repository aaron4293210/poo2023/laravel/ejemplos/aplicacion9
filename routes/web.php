<?php

use App\Http\Controllers\front\NoticiaController as FrontNoticiaController;
use App\Http\Controllers\back\NoticiaController as BackNoticiaController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

// Pagina de inicio frontend
// Quiero listar todas las noticias con solo algunos campos y cuand pinche en la noticia me muestre la noticia completa
Route::controller(FrontNoticiaController::class)->group(function () {
    Route::get('/', 'index')->name('home');
    Route::get('/front/noticias/{noticia}', 'show')->name('front.noticias.show');
    Route::post('/front/noticias/buscar', 'searchNoticia')->name('front.noticias.searchNoticia');
});

// Los usuarios registrados pueden hacer esto
Route::middleware('auth')->group(function () {
    // Pagina de inicio backend
    // Si pulso sobre el icono del menu me lleve al front
    // Quiero ver informacion del usuario y un menu con gestion de noticias | Crear Noticias | Noticias del usuario |
    // Dropdown para cerrar sesion (perfil, cerrar sesion)
    Route::get('/back/index', function () {
        return view('back.index');
    })->name('back.index');

    // Gestion del perfil
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    // Noticias
    Route::controller(BackNoticiaController::class)->group(function () {
        // Para mostrar las noticias del usuario lougeado
        Route::get('/back/noticias/listado', 'listado')->name('back.noticias.listado');
        Route::get('/back/noticias/buscar', 'searchNoticia')->name('back.noticias.searchNoticia');
    });

    Route::resource('back/noticias', BackNoticiaController::class)->names('back.noticias');
});

require __DIR__.'/auth.php';
