<x-app-layout>
    <x-tarjetaback
        :registro="$noticia"
        modelo="noticias"
        titulo='Noticia'
        modalMensaje="¿Estás seguro de que quieres borrar esta noticia?"
    />
</x-app-layout>

@pushn('js')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            const showModal = (modalId) => {
                document.getElementById(modalId).classList.remove('hidden');
            };

            const hideModal = (modalId) => {
                document.getElementById(modalId).classList.add('hidden');
            };

            document.querySelectorAll('[data-modal-show]').forEach(button => {
                button.addEventListener('click', () => {
                    showModal(button.getAttribute('data-modal-show'));
                });
            });

            document.querySelectorAll('[data-modal-hide]').forEach(button => {
                button.addEventListener('click', () => {
                    hideModal(button.getAttribute('data-modal-hide'));
                });
            });
        });
    </script>
@endpushn
