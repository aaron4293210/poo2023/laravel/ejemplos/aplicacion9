<x-app-layout >
    <div class="container mx-auto p-6 mt-5 bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
        <form action="{{ route('back.noticias.update', $noticia) }}" method="POST"  enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="space-y-12">
                <div class="border-b border-gray-900/10 pb-12">
                    <h2 class="text-xl font-semibold leading-7 text-indigo-600 dark:text-indigo-400 border-b border-indigo-600 dark:border-indigo-400 pb-2 mb-6">Actualizar Noticia</h2>

                    <div class="mt-4">
                        <div class="relative">
                            <input type="text" id="titulo" aria-describedby="error_titulo" name="titulo" class="block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border dark:text-white dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('titulo') border-red-500 dark:border-red-500 @enderror" placeholder=" " value="{{ old('titulo', $noticia->titulo) }}"/>
                            <label for="titulo" class="absolute text-base text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-800 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">Título</label>
                        </div>

                        @error('titulo')
                            <p id="error_titulo" class="mt-2 text-sm text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</span></p>
                        @enderror
                    </div>

                    <div class="mt-4">
                        <div class="relative">
                            <textarea id="contenido" rows="3" aria-describedby="error_contenido" name="contenido" class="block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border dark:text-white dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('contenido') border-red-500 dark:border-red-500 @enderror" placeholder=" ">{{ old('contenido', $noticia->contenido) }}</textarea>
                            <label for="contenido" class="absolute text-base text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-800 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">Contenido</label>
                        </div>

                        @error('contenido')
                            <p id="error_contenido" class="mt-2 text-sm text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</span></p>
                        @enderror
                    </div>

                    <div class="col-span-full gap-y-8 mt-7">
                        <label for="cover-photo" class="block text-base font-medium leading-6 text-gray-900 dark:text-white">Foto de la noticia</label>

                        <div class="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10">
                            <div class="text-center">
                                <div id="preview" class="bg-cover w-full h-60">
                                    <img id="preview-image" class="w-full h-60" src="{{ asset('storage/' . $noticia->foto ) }}">
                                </div>

                                <div class="mt-4 flex text-sm leading-6 text-gray-600">
                                    <label for="foto" class="relative cursor-pointer rounded-md bg-white font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <div class="flex flex-col items-center justify-center">
                                            <span>Subir Imagen</span>
                                            <p class="text-xs leading-5 text-gray-600">PNG, JPG, up to 1MB</p>
                                        </div>
                                        <input type="file" id="foto" name="foto" class="sr-only">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-6 flex justify-center gap-x-6">
                    <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Actualizar</button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>

<script>
    document.getElementById('foto').addEventListener('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        var preview = document.getElementById('preview');
        var previewImage = document.getElementById('preview-image');

        if (file) {
            reader.onloadend = function() {
                previewImage.src = reader.result;
                previewImage.style.display = 'block';
                preview.style.backgroundImage = 'none';
            }
            reader.readAsDataURL(file);
        }
        else {
            previewImage.style.display = 'none';
            preview.style.backgroundImage = 'url("' + previewImage.src + '")';
            preview.style.backgroundSize = 'cover'; // Asegura que la imagen se ajuste al contenedor
        }
    });
</script>
