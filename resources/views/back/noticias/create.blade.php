<x-app-layout>
    <div class="container mx-auto p-6 mt-5 bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
        <form action="{{ route('back.noticias.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="space-y-12">
                <div class="border-b border-gray-900/10 pb-12">
                    <h2 class="text-xl font-semibold leading-7 text-indigo-600 dark:text-indigo-400 border-b border-indigo-600 dark:border-indigo-400 pb-2 mb-6">Crea tu Noticia</h2>

                    <div class="mt-4">
                        <div class="relative">
                            <input type="text" id="titulo" aria-describedby="error_titulo" name="titulo" class="block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border dark:text-white dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('titulo') border-red-500 dark:border-red-500 @enderror" placeholder=" " />
                            <label for="titulo" class="absolute text-base text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-800 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">Título</label>
                        </div>

                        @error('titulo')
                            <p id="error_titulo" class="mt-2 text-sm text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</span></p>
                        @enderror
                    </div>

                    <div class="mt-4">
                        <div class="relative">
                            <textarea id="contenido" rows="3" aria-describedby="error_contenido" name="contenido" class="block px-2.5 pb-2.5 pt-4 w-full text-lg text-gray-900 bg-transparent rounded-lg border dark:text-white dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('contenido') border-red-500 dark:border-red-500 @enderror" placeholder=" "></textarea>
                            <label for="contenido" class="absolute text-base text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-800 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto start-1">Contenido</label>
                        </div>

                        @error('contenido')
                            <p id="error_contenido" class="mt-2 text-sm text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</span></p>
                        @enderror
                    </div>

                    <div class="col-span-full mt-7">
                        <label for="cover-photo" class="block text-base font-medium leading-6 text-gray-900 dark:text-white">Foto de la noticia</label>

                        <div class="mt-2 flex justify-center rounded-lg border border-dashed border-gray-900/25 px-6 py-10 dark:border-gray-700">
                            <div class="text-center">
                                <div id="preview" class="bg-cover">
                                    <svg id="svg-icon" class="mx-auto h-12 w-12 text-gray-300 dark:text-gray-600" viewBox="0 0 24 24" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M1.5 6a2.25 2.25 0 012.25-2.25h16.5A2.25 2.25 0 0122.5 6v12a2.25 2.25 0 01-2.25 2.25H3.75A2.25 2.25 0 011.5 18V6zM3 16.06V18c0 .414.336.75.75.75h16.5A.75.75 0 0021 18v-1.94l-2.69-2.689a1.5 1.5 0 00-2.12 0l-.88.879.97.97a.75.75 0 11-1.06 1.06l-5.16-5.159a1.5 1.5 0 00-2.12 0L3 16.061zm10.125-7.81a1.125 1.125 0 112.25 0 1.125 1.125 0 01-2.25 0z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                                <div class="mt-4 flex text-sm leading-6 text-gray-600 dark:text-gray-400">
                                    <label for="foto" class="relative cursor-pointer rounded-md bg-white dark:bg-gray-800 font-semibold text-indigo-600 focus-within:outline-none focus-within:ring-2 focus-within:ring-indigo-600 focus-within:ring-offset-2 hover:text-indigo-500">
                                        <div class="flex flex-col items-center justify-center">
                                            <span>Subir Imagen</span>
                                            <p class="text-xs leading-5 text-gray-600 dark:text-gray-400">PNG, JPG, hasta 1MB</p>
                                        </div>

                                        <input type="file" id="foto" name="foto" class="sr-only">
                                    </label>
                                </div>
                            </div>
                        </div>

                        @error('foto')
                            <p class="mt-2 text-sm text-red-600 dark:text-red-400"><span class="font-medium">{{ $message }}</span></p>
                        @enderror
                    </div>
                </div>

                <div class="mt-6 flex justify-center gap-x-6">
                    <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Crear</button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>

<script>
    document.getElementById('foto').addEventListener('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();
        var preview = document.getElementById('preview');
        var svgIcon = document.getElementById('svg-icon');

        reader.onloadend = function() {
            preview.style.backgroundImage = 'url("' + reader.result + '")';
            preview.style.backgroundSize = 'contain';
            preview.style.backgroundRepeat = 'no-repeat';
            preview.style.backgroundPosition = 'center';
            preview.classList.add('w-64', 'h-64');
            svgIcon.style.display = 'none';
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.style.backgroundImage = 'none';
            preview.classList.remove('w-64', 'h-64');
            svgIcon.style.display = 'block';
        }
    });
</script>
