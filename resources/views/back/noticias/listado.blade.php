<x-app-layout>
    <div class="m-6">
        <x-alerta/>

        <form class="max-w-md mx-auto m-5" action="{{ route('back.noticias.searchNoticia') }}" method="GET">
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
            <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                    </svg>
                </div>
                <input type="search" id="default-search" name="query" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Buscar noticias" required />
                <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Buscar</button>
            </div>
        </form>

        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
            @if(count($noticias) != 0)
                @foreach ($noticias as $noticia)
                    <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700 overflow-hidden transition-transform duration-500 ease-out hover:scale-105 relative flex flex-col">
                        <div class="absolute top-0 right-0 p-2 bg-white bg-opacity-75 rounded-md">
                            <p class="text-xs text-gray-500">{{ $noticia->created_at->diffForHumans() }}</p>
                        </div>

                        @if ($noticia->foto != null)
                            <a href="{{ route('back.noticias.show', $noticia) }}">
                                    <img class="w-full h-48 object-cover" src="{{ asset('storage/' . $noticia->foto ) }}" alt="" />
                            </a>
                        @endif

                        <div class="p-5 flex-grow">
                            <a href="{{ route('back.noticias.show', $noticia) }}">
                                <h5 class="mb-2 text-xl font-semibold leading-tight text-gray-900 dark:text-white">{{ $noticia->titulo }}</h5>
                            </a>

                            <hr class="my-2 border-gray-200 dark:border-gray-600">

                            <div class="flex items-center mb-2 text-gray-600 dark:text-gray-400">
                                <svg class="h-4 w-4 mr-1.5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5z"></path>
                                </svg>
                                <p class="text-sm">{{ $noticia->user->name }}</p>
                            </div>

                            <p class="text-sm text-gray-700 dark:text-gray-400">{{ Str::limit($noticia->contenido, 100, '...') }}</p>
                        </div>

                        <div class="p-5">
                            <div class="flex justify-center items-center space-x-2">
                                <a href="{{ route('back.noticias.show', $noticia) }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-500 rounded-lg hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-400 dark:hover:bg-blue-500 dark:focus:ring-blue-600">
                                    Ver más
                                </a>
                                <a href="{{ route('back.noticias.edit', $noticia) }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-green-500 rounded-lg hover:bg-green-600 focus:ring-4 focus:outline-none focus:ring-green-300 dark:bg-green-400 dark:hover:bg-green-500 dark:focus:ring-green-600">
                                    Editar
                                </a>
                                <button data-modal-show="modalborrado-{{ $noticia->id }}" class="inline-flex justify-center items-center px-3 py-2 text-sm font-medium text-center text-white bg-red-500 rounded-lg hover:bg-red-600 focus:ring-4 focus:outline-none focus:ring-red-300 dark:bg-red-400 dark:hover:bg-red-500 dark:focus:ring-red-600">
                                    Borrar
                                </button>
                            </div>
                        </div>
                    </div>

                    <x-modalborrado
                        idModal="modalborrado-{{ $noticia->id }}"
                        mensaje="¿Estás seguro de que quieres borrar esta noticia?"
                        ruta="{{ route('back.noticias.destroy', $noticia) }}"
                    />
                @endforeach
            @else
                <p class="text-center text-lg font-semibold text-gray-700 dark:text-gray-300 mt-8">No hay noticias</p>
            @endif
        </div>

        <div class="mt-6">
            {{ $noticias->links() }}
        </div>
    </div>

    @push('js')
        <script>
            document.addEventListener('DOMContentLoaded', () => {
                const showModal = (modalId) => {
                    document.getElementById(modalId).classList.remove('hidden');
                };

                const hideModal = (modalId) => {
                    document.getElementById(modalId).classList.add('hidden');
                };

                document.querySelectorAll('[data-modal-show]').forEach(button => {
                    button.addEventListener('click', () => {
                        showModal(button.getAttribute('data-modal-show'));
                    });
                });

                document.querySelectorAll('[data-modal-hide]').forEach(button => {
                    button.addEventListener('click', () => {
                        hideModal(button.getAttribute('data-modal-hide'));
                    });
                });
            });
        </script>
    @endpush
</x-app-layout>
