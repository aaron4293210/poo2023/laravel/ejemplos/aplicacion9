<x-main-layout>
    <div>
        <form class="max-w-md mx-auto m-5" action="{{ route('front.noticias.searchNoticia') }}" method="POST">
            @csrf
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>

            <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                    </svg>
                </div>
                <input type="search" id="default-search" name="buscar" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Buscar noticias" />
                <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Buscar</button>
            </div>
        </form>
    </div>

    @if (count($noticias) > 0)
        @foreach ($noticias as $noticia)
            <a href="{{ route('front.noticias.show', $noticia) }}" class="container mx-auto">
                <div class="max-w-6xl mx-auto bg-white border border-gray-300 rounded-lg shadow-md hover:shadow-lg transition duration-300 ease-in-out hover:border-gray-400 hover:bg-gray-50 relative">
                    <div class="absolute top-0 right-0 p-2 bg-gray-200 bg-opacity-75 rounded-md border border-gray-300">
                        <p class="text-xs text-gray-700">{{ $noticia->created_at->diffForHumans() }}</p>
                    </div>
                    <div class="flex items-center">
                        <div class="w-full md:w-1/3 overflow-hidden">
                            @if ($noticia->foto != null)
                                <img class="rounded-l-lg md:rounded-l-none md:rounded-r-none w-full h-auto" src="{{ asset('storage/' . $noticia->foto ) }}" alt="Imagen de la noticia" />
                            @else
                                <p class="text-center">Sin imagen</p>
                            @endif
                        </div>
                        <div class="w-full md:w-2/3 p-6">
                            <div class="flex items-center justify-between">
                                <h2 class="text-2xl font-semibold leading-7 text-gray-900">{{ $noticia->titulo }}</h2>
                            </div>
                            <p class="mt-3 text-base leading-6 text-gray-700">{{ Str::limit($noticia->contenido, 150, '...') }}</p>
                            <div class="mt-4 text-sm text-gray-600 flex items-center justify-between">
                                <div>
                                    <svg class="h-4 w-4 mr-1.5 text-gray-400 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5z"></path>
                                    </svg>
                                    <p class="inline-block">Autor: {{ $noticia->user->name }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
    @else
        <p>No hay noticias</p>
    @endif

    {{ $noticias->links() }}
</x-main-layout>
