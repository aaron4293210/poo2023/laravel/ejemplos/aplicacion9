<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>

    <body class="font-sans antialiased dark:bg-black dark:text-white/50">
        <div class="bg-gray-50 text-black/50 dark:bg-black dark:text-white/50">
            {{-- <img id="background" class="absolute -left-20 top-0 max-w-[877px] pointer-events-none"src="https://laravel.com/assets/img/welcome/background.svg" /> --}}
            <x-menufront />
            <div class="relative min-h-screen flex flex-col items-center justify-center selection:bg-[#FF2D20] selection:text-white">
                <div class="relative w-full max-w-2xl px-6 lg:max-w-7xl">
                    {{-- Mostrar las noticias --}}
                    <main class="mt-6">
                        <div class="grid gap-6 lg:grid-cols-1 lg:gap-8">
                            {{ $slot }}
                        </div>
                    </main>
                    {{-- Fin mostrar noticias --}}

                    <footer class="py-16 text-center text-sm text-black dark:text-white/70">
                        Noticias
                    </footer>
                </div>
            </div>
        </div>
    </body>
</html>
