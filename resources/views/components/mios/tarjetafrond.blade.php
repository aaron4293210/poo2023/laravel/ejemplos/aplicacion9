@props([
    'registro',
    'modelo',
])

<div class="container mx-auto p-6 mt-5">
    <div class="max-w-6xl mx-auto bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
        <div class="flex items-center justify-center">
            <div class="w-1/3">
                @if ($registro->foto != null)
                    <img class="rounded-lg w-4/5 h-auto mx-auto" src="{{ asset('storage/' . $registro->foto ) }}" alt="Imagen de la noticia" />
                @else
                    <p class="text-center">Sin imagen</p>
                @endif
            </div>

            <div class="w-2/3 p-6">
                <div class="flex items-center justify-between">
                    <h2 class="text-xl font-semibold leading-7 text-gray-900">{{ $registro->titulo }}</h2>
                </div>
                <p class="mt-3 text-base leading-6 text-gray-700">{{ $registro->contenido }}</p>
                <div class="mt-6 text-sm text-gray-500">
                    @foreach ([
                        ['icon' => 'M4 6h16M4 10h16M4 14h16M4 18h16', 'text' => 'Fecha de creación: ' . $registro->created_at->format('d/m/Y H:i')],
                        ['icon' => 'M6 18L18 6M6 6l12 12', 'text' => 'Última actualización: ' . $registro->updated_at->format('d/m/Y H:i')],
                        ['icon' => 'M12 14l9-5-9-5-9 5 9 5z', 'text' => 'Autor: ' . $registro->user->name],
                    ] as $item)
                        <div class="flex items-center">
                            <svg class="h-4 w-4 mr-1.5 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="{{ $item['icon'] }}"></path>
                            </svg>
                            <p>{{ $item['text'] }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
